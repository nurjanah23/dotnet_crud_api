﻿using CrudWebApi.EntityFramworks;
using CrudWebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CrudWebApi.Controllers
{
    [RoutePrefix("api/productCustom")]
    public class ProductCustomController : ApiController
    {
        [Route("calculateProductUnitPrice")]
        [HttpPost]
        public IHttpActionResult calculateProductUnitPrice([FromBody] ProductDetailCalculationParam param)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    var temp = db.Products.AsQueryable();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var listProduct = db.Products.OrderByDescending(data => data.ProductID).ToList();

                    CalculationViewModel calculator = new CalculationViewModel(';');
                    foreach (var item in listProduct)
                    {
                        calculator.calculateProductUnitPrice(item, param);
                    }

                    db.SaveChanges();
                    return Ok("Data Saved Successfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("countInvalidProductDetail")]
        [HttpPost]
        public IHttpActionResult countInvalidProductDetail([FromBody] ProductDetailCalculationParam param)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    var temp = db.Products.AsQueryable();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var listProduct = db.Products.ToList();

                    CalculationViewModel calculator = new CalculationViewModel(';');
                    foreach (var item in listProduct)
                    {
                        calculator.calculateProductUnitPrice(item, param);
                    }

                    db.SaveChanges();
                    return Ok("Data Saved Successfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("createProductWithStringProductDetail")]
        [HttpPost]
        public IHttpActionResult createProductWithStringProductDetail([FromBody] ProductDetailCalculationParam param)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    var temp = db.Products.AsQueryable();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var listProduct = db.Products.ToList();

                    CalculationViewModel calculator = new CalculationViewModel(';');
                    foreach (var item in listProduct)
                    {
                        calculator.calculateProductUnitPrice(item, param);
                    }

                    db.SaveChanges();
                    return Ok("Data Saved Successfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("createProductWithProductDetail")]
        [HttpPost]
        public IHttpActionResult createProductWithProductDetail([FromBody] ProductDetailCalculationParam param)
        {
            try
            {
                using (var db = new DB_Context())
                {
                    var temp = db.Products.AsQueryable();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var listProduct = db.Products.ToList();

                    CalculationViewModel calculator = new CalculationViewModel(';');
                    foreach (var item in listProduct)
                    {
                        calculator.calculateProductUnitPrice(item, param);
                    }

                    db.SaveChanges();
                    return Ok("Data Saved Successfully");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}