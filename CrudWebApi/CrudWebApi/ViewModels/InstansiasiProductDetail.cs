﻿using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class InstansiasiProductDetail
    {
        private char del;

        public InstansiasiProductDetail(char del)
        {
            this.del = del;
        }
        public void productDetailInstansasi(Product product)
        {
            ProductDetailViewModel productDetail = null;
            if (product.ProductType != null)
            {
                if (product.ProductType.Equals("FoodAndBeverageItems"))
                {
                    productDetail = new FoodsAndBeverageItems(this.del, product);
                }
                else if (product.ProductType.Equals("MaterialItems"))
                {
                    productDetail = new MaterialItems(this.del, product);
                }
                else if (product.ProductType.Equals("GarmentItems"))
                {
                    productDetail = new GarmentItems(this.del, product);
                }
                else if (product.ProductType.Equals("TransportationServices"))
                {
                    productDetail = new TransportationServices(this.del, product);
                }
                else if (product.ProductType.Equals("TelecommunicationServices"))
                {
                    productDetail = new TelecomunicationServices(this.del, product);
                }
                else
                {
                    throw new Exception("Unknown Product Type");
                }
            }

        }
    }
}