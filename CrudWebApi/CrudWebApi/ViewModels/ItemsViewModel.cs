﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public interface IItemsViewModel
    {
        char delimeter(char? del = null);
        Dictionary<string, object> Prod();
        string ConvertToString();

        decimal calculateProductUnitPrice();

    }
}