﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    interface IProductDetail
    {
        string ConvertToString();
        Dictionary<string, object> ConvertToDictionary();
    }
}