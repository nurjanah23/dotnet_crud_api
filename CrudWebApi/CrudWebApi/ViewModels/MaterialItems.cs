﻿using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class MaterialItems : ItemDelimeter
    {

        public string expiredDate { get; set; }
        public string materialsType { get; set; }
        public string isConsumable { get; set; }

        public MaterialItems(char del, Product product) : base(del)
        {
            this.productID = product.ProductID;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                string[] prod = product.ProductDetail.Split(this.del);
                this.productDescription = prod[0];
                this.productionCode = prod[1];
                this.productionDate = prod[2];
                this.expiredDate = prod[3];
                this.materialsType = prod[4];
                this.isConsumable = prod[5];
                this.unitOfMeasurent = prod[6];
                this.costRate = prod[7];
            }
        }
        public override Dictionary<string, object> ConvertToDictionary()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("productID", this.productID);
            result.Add("productDescription", this.productDescription);
            result.Add("productionCode", this.productionCode);
            result.Add("productionDate", this.productionDate);
            result.Add("expiredDate", this.expiredDate);
            result.Add("materialsType", this.materialsType);
            result.Add("isConsumable", this.isConsumable);
            result.Add("unitOfMeasurent", this.unitOfMeasurent);
            result.Add("costRate", this.costRate);

            return result;
        }

        public override string ConvertToString()
        {
            return this.appendWithDelimeter(
                 this.productDescription, this.productionCode, this.productionDate, this.expiredDate, this.materialsType,
                 this.isConsumable, this.unitOfMeasurent, this.costRate);
        }

    }
}