﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public interface IServicesViewModel
    {
        char delimeter(char? del = null);
        Dictionary<string, object> Prod();
        string ConvertToString();

        decimal? costCalculate(string condition = "", int? userDemand = 0, decimal? duration = 0);
    }
}