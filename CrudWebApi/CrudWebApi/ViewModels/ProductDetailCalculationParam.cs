﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class ProductDetailCalculationParam
    {
        public int? userDemand { get; set; }
        public int? wheater { get; set; }
        public int? duration { get; set; }
        public int getNotNullUserDemand()
        {
            return this.userDemand ?? 0;
        }

        public int getNotNullDuration()
        {
            return this.duration ?? 0;
        }
    }
}