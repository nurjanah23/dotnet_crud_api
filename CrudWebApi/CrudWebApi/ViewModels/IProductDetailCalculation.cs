﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    interface IProductDetailCalculation
    {
        void setParam(ProductDetailCalculationParam param);
        decimal calculateProductCost();
    }
}