﻿using AutoMapper;
using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public abstract class ProductDetailViewModel : IProductDetail, IProductDetailCalculation
    {

        public readonly decimal rateCalculation = (decimal)(110.00 / 100.00);
        public int productID { get; set; }
        public string productDescription { get; set; }
        public string costRate { get; set; }
        public char del { get; set; }

        public ProductDetailViewModel(char del)
        {
            this.del = del;
        }

        public decimal getCostRate()
        {
            return this.rateCalculation;
        }

        public ProductDetailViewModel()
        {

        }
        public string appendWithDelimeter(params object[] listParam)
        {
            string result = "";
            char del = (char)0;

            foreach(object param in listParam)
            {
                result += del + param.ToString();
                del = this.del;
            }

            return result;
        }

        public abstract string ConvertToString();
        public abstract Dictionary<string, object> ConvertToDictionary();
        public abstract void setParam(ProductDetailCalculationParam param);
        public abstract decimal calculateProductCost();

    }
}