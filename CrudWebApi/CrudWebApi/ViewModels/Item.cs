﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public abstract class Item : ProductDetailViewModel
    {
        public Item(char del) : base(del)
        {

        }

        public string productionCode { get; set; }
        public string productionDate { get; set; }
        public string unitOfMeasurent { get; set; }

        public override void setParam(ProductDetailCalculationParam param)
        {
        }

        public override decimal calculateProductCost()
        {
            return decimal.Parse(this.costRate);
        }
    }
}