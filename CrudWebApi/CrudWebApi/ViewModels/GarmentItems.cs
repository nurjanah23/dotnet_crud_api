﻿using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class GarmentItems : ItemDelimeter
    {
        public string garmentType { get; set; }
        public string Fabrics { get; set; }
        public string genderRelated { get; set; }
        public string isWaterProof { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string ageGroup { get; set; }

        public GarmentItems(char del, Product product) : base(del)
        {
            this.productID = product.ProductID;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                string[] prod = product.ProductDetail.Split(this.del);
                this.productDescription = prod[0];
                this.productionCode = prod[1];
                this.productionDate = prod[2];
                this.garmentType = prod[3];
                this.Fabrics = prod[4];
                this.genderRelated = prod[5];
                this.isWaterProof = prod[6];
                this.Color = prod[7];
                this.Size = prod[8];
                this.ageGroup = prod[9];
                this.unitOfMeasurent = prod[10];
                this.costRate = prod[11];
            }
        }

        public override Dictionary<string, object> ConvertToDictionary()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("productID", this.productID);
            result.Add("productDescription", this.productDescription);
            result.Add("productionCode", this.productionCode);
            result.Add("productionDate", this.productionDate);
            result.Add("garmentType", this.garmentType);
            result.Add("Fabrics", this.Fabrics);
            result.Add("genderRelated", this.genderRelated);
            result.Add("isWaterProof", this.isWaterProof);
            result.Add("Color", this.Color);
            result.Add("Size", this.Size);
            result.Add("ageGroup", this.ageGroup);
            result.Add("unitOfMeasurent", this.unitOfMeasurent);
            result.Add("costRate", this.costRate);

            return result;
        }

        public override string ConvertToString()
        {
            return this.appendWithDelimeter(
                this.productDescription, this.productionCode, this.productionDate, this.garmentType, this.Fabrics,
                this.genderRelated, this.isWaterProof, this.Color, this.Size, this.ageGroup, this.unitOfMeasurent, this.costRate);
        }

    }
}