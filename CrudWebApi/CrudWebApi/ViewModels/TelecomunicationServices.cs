﻿using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class TelecomunicationServices : ServiceDelimeter
    {
        public string packetType { get; set; }
        public string packetLimit { get; set; }

        public TelecomunicationServices(char del, Product product) : base(del)
        {
            this.productID = product.ProductID;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                string[] prod = product.ProductDetail.Split(this.del);
                this.productDescription = prod[0];
                this.packetType = prod[1];
                this.packetLimit = prod[2];
                this.costCalculationMethod = prod[3];
                this.costRate = prod[4];
            }
        }

        public override Dictionary<string, object> ConvertToDictionary()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("productID", this.productID);
            result.Add("productDescription", this.productDescription);
            result.Add("packetType", this.packetType);
            result.Add("packetLimit", this.packetLimit);
            result.Add("costCalculationMehod", this.costCalculationMethod);
            result.Add("costRate", this.costRate);

            return result;
        }

        public override string ConvertToString()
        {
            return this.appendWithDelimeter(this.productDescription, this.packetType, this.packetLimit, this.costCalculationMethod, this.costRate);
        }

        public override decimal calculateProductCost()
        {
            decimal DecCostRate = decimal.Parse(costRate);
            var temp = this.param;

            if (costCalculationMethod.Equals("PerSecond"))
            {
                return DecCostRate * this.param.getNotNullDuration();
            }
            else if (costCalculationMethod.Equals("PerPacket"))
            {
                if (packetType.Equals("Data"))
                {
                    return decimal.Parse(packetLimit) * DecCostRate;
                }
                else
                {
                    return DecCostRate * this.param.getNotNullDuration();
                }
            }
            else
            {
                return 0;
            }
        }
       
    }
}