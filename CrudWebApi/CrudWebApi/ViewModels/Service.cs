﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public abstract class Service : ProductDetailViewModel
    {
        public ProductDetailCalculationParam param { get; set; }

        public Service(char del) : base(del)
        {
        }

        public string costCalculationMethod { get; set; }

        public override void setParam(ProductDetailCalculationParam param)
        {
            this.param = param;
        }
    }
}