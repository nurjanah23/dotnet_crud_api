﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public abstract class ItemDelimeter : Item
    {
        public ItemDelimeter (char del = '|'): base(del)
        {

        }
    }
}