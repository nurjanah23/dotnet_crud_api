﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public abstract class ServiceDelimeter : Service
    {
        public ServiceDelimeter(char del = ';') : base(del)
        {

        }
    }
}