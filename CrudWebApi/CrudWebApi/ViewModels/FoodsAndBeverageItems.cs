﻿using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class FoodsAndBeverageItems : ItemDelimeter
    {
        public string expiredDate { get; set; }
        public string netWeight { get; set; }
        public string Ingredients { get; set; }
        public string dailyValue { get; set; }
        public string Certification { get; set; }

        
        public FoodsAndBeverageItems(char del , Product product) : base(del)
        {
            this.productID = product.ProductID;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                string[] prod = product.ProductDetail.Split(this.del);
                this.productDescription = prod[0];
                this.productionCode = prod[1];
                this.productionDate = prod[2];
                this.expiredDate = prod[3];
                this.netWeight = prod[4];
                this.Ingredients = prod[5];
                this.dailyValue = prod[6];
                this.Certification = prod[7];
                this.unitOfMeasurent = prod[8];
                this.costRate = prod[9];
            }
        }

        public FoodsAndBeverageItems(char del, Dictionary<string, object> dictionary) : base(del)
        {
            this.Certification = dictionary["Certification"].ToString();
            this.expiredDate = dictionary["expiredDate"].ToString();
            this.netWeight = dictionary["netWeight"].ToString();
            this.Ingredients = dictionary["Ingredients"].ToString();
            this.dailyValue = dictionary["dailyValue"].ToString();
            this.unitOfMeasurent = dictionary["unitOfMeasurent"].ToString();
            this.costRate = dictionary["costRate"].ToString();
            this.productionCode = dictionary["productionCode"].ToString();
            this.productionDate = dictionary["productionDate"].ToString();
            this.productDescription = dictionary["productDescription"].ToString();
        }

        public override Dictionary<string, object> ConvertToDictionary()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("productID", this.productID);
            result.Add("productDescription", this.productDescription);
            result.Add("productionCode", this.productionCode);
            result.Add("productionDate", this.productionDate);
            result.Add("expiredDate", this.expiredDate);
            result.Add("netWeight", this.netWeight);
            result.Add("Ingredients", this.Ingredients);
            result.Add("dailyValue", this.dailyValue);
            result.Add("Certification", this.Certification);
            result.Add("unitOfMeasurent", this.unitOfMeasurent);
            result.Add("costRate", this.costRate);

            return result;
        }

        public override string ConvertToString()
        {
            return this.appendWithDelimeter(
                this.productDescription, this.productionCode, this.productionDate,
                this.netWeight, this. Ingredients, this.Certification, this.unitOfMeasurent, this.costRate);
        }

    }
}