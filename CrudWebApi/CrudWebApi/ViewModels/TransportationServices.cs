﻿using CrudWebApi.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudWebApi.ViewModels
{
    public class TransportationServices : ServiceDelimeter
    {
        public string vehicleType { get; set; }
        public string routePath { get; set; }
        public string routeMilleage { get; set; }
        private object[] parameter { get; set; }

        public TransportationServices(char del, Product product) : base(del)
        {
            this.productID = product.ProductID;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                string[] prod = product.ProductDetail.Split(this.del);
                this.productDescription = prod[0];
                this.vehicleType = prod[1];
                this.routePath = prod[2];
                this.routeMilleage = prod[3];
                this.costCalculationMethod = prod[4];
                this.costRate = prod[5];
            }
        }

        public override Dictionary<string, object> ConvertToDictionary()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            result.Add("productID", this.productID);
            result.Add("productDescription", this.productDescription);
            result.Add("vehicleType", this.vehicleType);
            result.Add("routePath", this.routePath);
            result.Add("routeMilleage", this.routeMilleage);
            result.Add("costCalculationMethod", this.costCalculationMethod);
            result.Add("costRate", this.costRate);

            return result;
        }

        public override string ConvertToString()
        {
            return this.appendWithDelimeter(this.productDescription, this.vehicleType, this.routePath, this.routeMilleage, this.costCalculationMethod, this.costRate);
        }

        public override decimal calculateProductCost()
        {
            decimal decCostRate = decimal.Parse(this.costRate);
            decimal decRouteMilleage = decimal.Parse(this.routeMilleage);

            if (this.costCalculationMethod.Equals("FixPerRoute"))
            {
                return decCostRate;
            }
            else if (this.costCalculationMethod.Equals("PerMiles"))
            {
                return (decRouteMilleage * decCostRate) / 2;
            }
            else if (this.costCalculationMethod.Equals("PerMilesWithCondition"))
            {
                var conditon = 0;

                if (this.param.wheater != null)
                {
                    if (this.param.wheater.Equals("GoodWheater"))
                    {
                        conditon = 5;
                    }
                    else
                    {
                        conditon = 15;
                    }
                }
                return ((decRouteMilleage* decCostRate) / 2) * ((conditon + (this.param.getNotNullUserDemand() / 50)) + 95) / 100;
            }
            else
            {
                return 0;
            }
        }
        
    }
}