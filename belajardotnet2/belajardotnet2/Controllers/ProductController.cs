﻿using belajardotnet2.EntityFramworks;
using belajardotnet2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace belajardotnet2.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        [Route("readAll")]
        [HttpGet]
        public IHttpActionResult readAll()
        {
            using (var db = new DB_Context())
            {
                try
                {
                    var listProductEntity = db.Products.ToList();
                    List<ProductViewModel> listProduct = new List<ProductViewModel>();

                    Dictionary<string, object> result = new Dictionary<string, object>();
                    foreach (var item in listProductEntity)
                    {
                        ProductViewModel product = new ProductViewModel()
                        {
                            ProductID = item.ProductID,
                            ProductName = item.ProductName,
                            QuantityPerUnit = item.QuantityPerUnit,
                            UnitPrice = item.UnitPrice,
                            UnitsInStock = item.UnitsInStock,
                            UnitsOnOrder = item.UnitsOnOrder,
                            ReorderLevel = item.ReorderLevel,
                            Discontinued = item.Discontinued,
                            SupplierID = item.SupplierID,
                            CategoryID = item.CategoryID,
                        };

                        listProduct.Add(product);
                    };
                    result.Add("Message", "Read data success");
                    result.Add("Data", listProduct);
                    db.Dispose();
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        [Route("create")]
        [HttpPost]

        public IHttpActionResult Create([FromBody] ProductViewModel item)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Product newProduct = new Product()
                    {
                        ProductID = item.ProductID,
                        ProductName = item.ProductName,
                        QuantityPerUnit = item.QuantityPerUnit,
                        UnitPrice = item.UnitPrice,
                        UnitsInStock = item.UnitsInStock,
                        UnitsOnOrder = item.UnitsOnOrder,
                        ReorderLevel = item.ReorderLevel,
                        Discontinued = item.Discontinued,
                        SupplierID = item.SupplierID,
                        CategoryID = item.CategoryID,
                    };
                    db.Products.Add(newProduct);
                    db.SaveChanges();
                    result.Add("Message", "Insert Data Success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        [Route("update")]
        [HttpPut]

        public IHttpActionResult Update([FromBody] ProductViewModel item)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Product product = db.Products.Find(item.ProductID);
                    product.ProductID = item.ProductID;
                    product.ProductName = item.ProductName;
                    product.QuantityPerUnit = item.QuantityPerUnit;
                    product.UnitPrice = item.UnitPrice;
                    product.UnitsInStock = item.UnitsInStock;
                    product.UnitsOnOrder = item.UnitsOnOrder;
                    product.ReorderLevel = item.ReorderLevel;
                    product.Discontinued = item.Discontinued;
                    product.SupplierID = item.SupplierID;
                    product.CategoryID = item.CategoryID;

                    db.SaveChanges();

                    result.Add("Message", "Update Data Success");
                    return Ok(result);

                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int ProductID)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Product product = db.Products.Where(data => data.ProductID == ProductID).FirstOrDefault();
                    db.Products.Remove(product);
                    db.SaveChanges();
                    result.Add("Message", "Delete Data Success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                };
            }
        }

        [Route("custom")]
        [HttpGet]
        public IHttpActionResult Custom()
        {
            using (var db = new DB_Context())
            {
                try
                {
                    //var listProductEntity = db.Products.ToList();
                    List<ProductViewModel> listProduct = new List<ProductViewModel>();
                    Dictionary<string, object> result = new Dictionary<string, object>();


                    var palingMahal = db.Products.Where(data => data.UnitPrice == db.Products.Max( item => item.UnitPrice)).ToList().Select(maksimal => new ProductViewModel(maksimal));
                    var palingMurah = db.Products.Where(data => data.UnitPrice == db.Products.Min(item => item.UnitPrice)).ToList().Select(minimal => new ProductViewModel(minimal));
                    var kurangDari = db.Products.Where(data => data.UnitPrice < db.Products.Average(item => item.UnitPrice)).ToList().Select(x => new ProductViewModel(x));
                    var lebihDari = db.Products.Where(data => data.UnitPrice > db.Products.Average(item => item.UnitPrice)).ToList().Select(x => new ProductViewModel(x));
                    var SamaDengan = db.Products.Where(data => data.UnitPrice == db.Products.Average(item => item.UnitPrice)).ToList().Select(x => new ProductViewModel(x));

                    result.Add("Harga Paling Mahal", palingMahal);
                    result.Add("Harga Paling Murah", palingMurah);
                    result.Add("Harga Yang Kurang Dari Rata-Rata", kurangDari);
                    result.Add("Harga Yang Lebih Dari Rata-Rata", lebihDari);
                    result.Add("Harga Yang Sama Dengan Rata-Rata", SamaDengan);

                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        [Route("filterData")]
        [HttpGet]

        ///parameter bisa di set null...  ? di type data angka supaya bisa di insert null
        ///di pi biar ada categoryName di tambahkan categoryName di view model product
        ///atau buat view di data base, nanti di entity nya yang di akses view nya
        ///view di data base biasanya hanya untuk read aja
        ///store prosedur

        public IHttpActionResult filterData(string productName = null,string CategoryName = null, decimal? harga = null )
        {
            using (var db = new DB_Context())
            {
                try
                {
                    List<ProductViewModel> listProduct = new List<ProductViewModel>();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var tamp = db.Products.AsQueryable(); ///belum berbentuk data 

                    if( productName != null)
                    {
                        tamp = tamp.Where(data => data.ProductName.Contains(productName.ToLower()));
                    }

                    if (CategoryName != null)
                    {
                        tamp = tamp.Where(data => data.Category.CategoryName.ToLower().Contains(CategoryName.ToLower()));
                    }

                    if (harga != null)
                    {
                        tamp = tamp.Where(data => data.UnitPrice < harga);
                    }

                    var listProuct = tamp.ToList().Select(x => new ProductViewModel(x)); // sudah dirubah menjadi emaruilable (sudah menjadi data)

                    //var namaCategory = db.Products.Join(db.Categories, c => c.CategoryID, d => d.ategoryI);

                    //var palingMahal = db.Products.Where( b => b.ProductName from a in listProduct where a.Contains(data) select a);

                    //var palingMahal = from c in db.Products where c.ProductName.Contains(data) select c;
                    //var hasil = palingMahal.ToList().Select(x => new ProductViewModel(x));
                    //var t = fileList.Where(file => filterList.Any(folder => file.ToUpperInvariant().Contains(folder.ToUpperInvariant())));

                    result.Add("Hasil Pencarian", listProduct);

                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
    }
}