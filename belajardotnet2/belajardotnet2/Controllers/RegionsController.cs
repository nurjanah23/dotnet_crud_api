﻿using belajardotnet2.EntityFramworks;
using belajardotnet2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace belajardotnet2.Controllers
{
    [RoutePrefix("api/regions")]
    public class RegionsController : ApiController
    {
        [Route("readAll")]
        [HttpGet]
        public IHttpActionResult readAll()
        {
            using (var db = new DB_Context())
            {
                try
                {
                    var listRegion = db.Regions.ToList();
                    List<RegionViewModel> dataRegion = new List<RegionViewModel>();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    foreach (var item in listRegion)
                    {
                        if (item.RegionDescription.Contains('|'))
                        {
                            RegionViewModel ListDataRegion = new RegionViewModel(item);
                            dataRegion.Add(ListDataRegion);
                        }
                        else
                        {
                            RegionViewModel ListDataRegion = new RegionViewModel()
                            {
                                RegionID = item.RegionID,
                                RegionName = item.RegionDescription
                            };
                            dataRegion.Add(ListDataRegion);
                        }


                    }

                    result.Add("Message", "Data Berhasil Ditampilkan");
                    result.Add("Data", dataRegion);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [Route("Create")]
        [HttpPost]
        public IHttpActionResult create([FromBody] RegionDetail dataBody)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var newRegion = dataBody.convertToRegion();
                    db.Regions.Add(newRegion);
                    db.SaveChanges();
                    dataBody.tambahTerritory(db);
                    var listRegion = db.Regions.Where(data => data.RegionID == dataBody.RegionID).AsEnumerable().ToList();
                    List<RegionDetail> dataRegion = new List<RegionDetail>();
                    foreach (var item in listRegion)
                    {
                        RegionDetail ListDataRegion = new RegionDetail(item);
                        dataRegion.Add(ListDataRegion);
                    }
                    result.Add("Data", dataRegion);
                    
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
    }
}