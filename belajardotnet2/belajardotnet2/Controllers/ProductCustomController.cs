﻿using belajardotnet2.EntityFramworks;
using belajardotnet2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace belajardotnet2.Controllers
{
    [RoutePrefix("api/productCustom")]
    public class ProductCustomController : ApiController
    {
        [Route("create")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] ProductDetailViewModel dataBody)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    product data = dataBody.convertToProduct();
                    db.Products.Add(data);
                    db.SaveChanges();
                    result.Add("Message", "Insert Data Success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int ProductID)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    product product = db.Products.Where(data => data.ProductID == ProductID).FirstOrDefault();
                    db.Products.Remove(product);
                    db.SaveChanges();
                    result.Add("Message", "Delete Data Success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                };
            }
        }

        [Route("getTransportationCostCalculation")]
        [HttpGet]
        public IHttpActionResult getTransportationCostCalculation(string condition, int userDemand)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    var tem = db.Products.AsQueryable();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    List<CostCalculationViewModel> listProduct = new List<CostCalculationViewModel>();
                    tem = tem.Where(data => data.ProductType.Contains("TransportationServices"));
                    var listData= tem.AsEnumerable().ToList();
                    foreach (var item in listData)
                    {
                        CostCalculationViewModel product = new CostCalculationViewModel(item, condition, userDemand);
                        listProduct.Add(product);
                    }
                    result.Add("Message", "Sukses");
                    result.Add("Data", listProduct);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }

            }
        }

        [Route("readAll")]
        [HttpGet]
        public IHttpActionResult readAll(int? productID = null)
        {
            using (var db = new DB_Context())
            {
                var Data = db.Products.AsQueryable();
                List<ProductDetailViewModel> dataDetail = new List<ProductDetailViewModel>();
                Dictionary<string, object> result = new Dictionary<string, object>();
                if (productID != null)
                {
                    Data = db.Products.Where(data => data.ProductID == productID);
                }
                var productDetail = Data.AsEnumerable().ToList();
                
                foreach (var item in productDetail)
                {
                   
                    ProductDetailViewModel ListData = new ProductDetailViewModel(item);
                    dataDetail.Add(ListData);


                }

                result.Add("Message", "Data Berhasil Ditampilkan");
                result.Add("Data", dataDetail);
                return Ok(result);

            }
        }
    }
}