﻿using belajardotnet2.EntityFramworks;
using belajardotnet2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace belajardotnet2.Controllers
{
    [RoutePrefix("api/Category")]
    public class CategoryController : ApiController
    {

        [Route("readAll")]
        [HttpGet]
        public IHttpActionResult ReadAll()
        {
            using (var db = new DB_Context())
            {
                try
                {
                    var listCategoryEntity = db.Categories.ToList();
                    List<CategoryViewModels> listCategory = new List<CategoryViewModels>();
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    foreach (var item in listCategoryEntity)
                    {
                        CategoryViewModels category = new CategoryViewModels()
                        {
                            CategoryID = item.CategoryID,
                            CategoryName = item.CategoryName,
                            Description = item.Description,
                            Picture = item.Picture
                        };

                        listCategory.Add(category);
                    };
                    result.Add("Message", "Read data success");
                    result.Add("Data", listCategory);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
               
            
        }
        
        [Route("create")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] CategoryViewModels dataBody)
        {
            var db = new DB_Context();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Category newCategory = new Category()
                {
                    CategoryID = dataBody.CategoryID,
                    CategoryName = dataBody.CategoryName,
                    Description = dataBody.Description,
                    Picture = dataBody.Picture
                };
                db.Categories.Add(newCategory);
                db.SaveChanges();
                db.Dispose();
                result.Add("Message", "Insert Data Success");
                return Ok(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Route("update")]
        [HttpPut]

        public IHttpActionResult Update([FromBody] CategoryViewModels dataBody)
        {
            var db = new DB_Context();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Category category = db.Categories.Find(dataBody.CategoryID);
                category.CategoryID = dataBody.CategoryID;
                category.CategoryName = dataBody.CategoryName;
                category.Description = dataBody.Description;
                category.Picture = dataBody.Picture;

                db.SaveChanges();
                db.Dispose();
                result.Add("Message", "Update Data Success");
                return Ok(result);

            }
            catch (Exception)
            {
                throw;
            }
        }


        [Route("delete")]
        [HttpDelete]

        public IHttpActionResult Delete(int CategoryID)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    Category category = db.Categories.Where(data => data.CategoryID == CategoryID).FirstOrDefault();
                    db.Categories.Remove(category);
                    db.SaveChanges();
                    result.Add("Message", "Delete Data Success");
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                };
            }
        }
    }


}