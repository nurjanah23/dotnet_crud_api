﻿using belajardotnet2.EntityFramworks;
using belajardotnet2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace belajardotnet2.Controllers
{
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        [Route("filterData")]
        [HttpGet]

        public IHttpActionResult filterData( String customerID)
        {
            using (var db = new DB_Context())
            {
                try
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    var order = db.Customers.AsQueryable().Where(data => data.CustomerID == customerID).AsEnumerable().ToList();

                    //var orderDetail = order.AsEnumerable().ToList(); // data orders di jadikan list

                    List<CustomerViewDetail> listOrderDetailCustamer = new List<CustomerViewDetail>();

                    foreach (var item in order) // perulangan dari data yang di dapat (list order detail)
                    {
                        CustomerViewDetail productListDetail = new CustomerViewDetail(item);
                        listOrderDetailCustamer.Add(productListDetail);

                    }

                    result.Add("Message", "berhasil");
                    result.Add("Hasil", listOrderDetailCustamer);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }
}