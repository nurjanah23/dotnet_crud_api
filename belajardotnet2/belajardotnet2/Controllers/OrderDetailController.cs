﻿using belajardotnet2.EntityFramworks;
using belajardotnet2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace belajardotnet2.Controllers
{
    [RoutePrefix("api/orderDetail")]
    public class OrderDetailController : ApiController
    {
        //[Route("readAll")]
        //[HttpGet]
        //public IHttpActionResult readAll()
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {
        //            var listOrderDetailEntity = db.Order_Details.ToList();
        //            List<OrderDetailViewModel> listOrderDetail = new List<OrderDetailViewModel>();

        //            Dictionary<string, object> result = new Dictionary<string, object>();
        //            foreach (var data in listOrderDetailEntity)
        //            {
        //                OrderDetailViewModel orderDetail = new OrderDetailViewModel()
        //                {
        //                    OrderID = data.OrderID,
        //                    ProductID = data.ProductID,
        //                    UnitPrice = data.UnitPrice,
        //                    Quantity = data.Quantity,
        //                    Discount = data.Discount
        //                };

        //                listOrderDetail.Add(orderDetail);
        //            };
        //            result.Add("Message", "Read data success");
        //            result.Add("Data", listOrderDetail);
        //            return Ok(result);
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }

        //}

        //[Route("create")]
        //[HttpPost]

        //public IHttpActionResult Create([FromBody] OrderDetailViewModel item = null)
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {
        //            Dictionary<string, object> result = new Dictionary<string, object>();
        //            Order_Detail orderDetail = new Order_Detail()
        //            {
        //                OrderID = item.OrderID,
        //                ProductID = item.ProductID,
        //                UnitPrice = item.UnitPrice,
        //                Quantity = item.Quantity,
        //                Discount = item.Discount
        //            };
        //            db.Order_Details.Add(orderDetail);
        //            db.SaveChanges();
        //            result.Add("Message", "Insert Data Success");
        //            return Ok(result);
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        //}
        //[Route("update")]
        //[HttpPut]

        //public IHttpActionResult Update([FromBody] OrderDetailViewModel item)
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {
        //            Dictionary<string, object> result = new Dictionary<string, object>();
        //            Order_Detail orderDetail = db.Order_Details.Find(item.OrderID);

        //            orderDetail.OrderID = item.OrderID;
        //            orderDetail.ProductID = item.ProductID;
        //            orderDetail.UnitPrice = item.UnitPrice;
        //            orderDetail.Quantity = item.Quantity;
        //            orderDetail.Discount = item.Discount;

        //            db.SaveChanges();

        //            result.Add("Message", "Update Data Success");
        //            return Ok(result);

        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }

        //}

        //[Route("delete")]
        //[HttpDelete]

        //public IHttpActionResult Delete(int orderID)
        //{
        //    using (var db = new DB_Context())
        //    {
        //        try
        //        {
        //            Dictionary<string, object> result = new Dictionary<string, object>();
        //            Order_Detail orderDetail = db.Order_Details.Where(data => data.OrderID == orderID).FirstOrDefault();
        //            db.Order_Details.Remove(orderDetail);
        //            db.SaveChanges();
        //            result.Add("Message", "Delete Data Success");
        //            return Ok(result);
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        };
        //    }
        //}
        
        [Route("filterData")]
        [HttpGet]

        ///parameter bisa di set null...  ? di type data angka supaya bisa di insert null
        ///di pi biar ada categoryName di tambahkan categoryName di view model product
        ///atau buat view di data base, nanti di entity nya yang di akses view nya
        ///view di data base biasanya hanya untuk read aja
        ///store prosedur

        public IHttpActionResult filterData(int? orderID = null)
        {
            using (var db = new DB_Context())
            {
                try
                {   
                    Dictionary<string, object> result = new Dictionary<string, object>(); 
                    List<OrderDetailViewModel> listOrderDetail = new List<OrderDetailViewModel>();
                    var order = db.Orders.AsQueryable(); // mengambil data dari tabel orders

                    if ( orderID != null) // jika param tidak sama dengan null
                    {
                        order = db.Orders.Where(data => data.OrderID == orderID); // jika ordeID yang di tabel orders di sama dengan parameter (orderID)
                    }
                    var orderDetail = order.AsEnumerable().ToList(); // data orders di jadikan list

                    foreach (var item in orderDetail) // perulangan dari data yang di dapat (list order detail)
                    {
                        OrderDetailViewModel productListDetail = new OrderDetailViewModel(item); // objek dari OrderDetailViewModel
                        listOrderDetail.Add(productListDetail); // memasukan data ke list listOrderDetail

                    }

                    result.Add("Message", "berhasil");
                    result.Add("Hasil", listOrderDetail);
                    return Ok(result);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
    }

}