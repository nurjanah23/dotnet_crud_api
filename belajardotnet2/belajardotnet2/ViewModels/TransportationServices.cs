﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class TransportationServices
    {
        int ProductID { get; set; }
        string ProductDescription { get; set; }
        string VehicleType { get; set; }
        string RoutePath { get; set; }
        string RouteMileage { get; set; }
        string CostCalculationMethod { get; set; }
        string CostRate { get; set; }

        public TransportationServices()
        {

        }

        public Dictionary<string, object> transportation()
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("ProductID", this.ProductID);
            data.Add("ProductDescription", this.ProductDescription);
            data.Add("VehicleType", this.VehicleType);
            data.Add("RoutePath", this.RoutePath);
            data.Add("RouteMileage", this.RouteMileage);
            data.Add("CostCalculationMethod", this.CostCalculationMethod);
            data.Add("CostRate", this.CostRate);
            return data;
        }

        public TransportationServices(product product)
        {
            char[] delimiter = { '|' };
            this.ProductID = product.ProductID;

            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] dataProduct = product.ProductDetail.Split(delimiter);

                this.ProductDescription = dataProduct[0];
                this.VehicleType = dataProduct[1];
                this.RoutePath = dataProduct[2];
                this.RouteMileage = dataProduct[3];
                this.CostCalculationMethod = dataProduct[4];
                this.CostRate = dataProduct[5];
            }
        }

        public string detailData()
        {
            return
                this.ProductDescription + "|" +
                this.VehicleType + "|" +
                this.RoutePath + "|" +
                this.RouteMileage + "|" +
                this.CostCalculationMethod + "|" +
                this.CostRate;

        }


    }
}