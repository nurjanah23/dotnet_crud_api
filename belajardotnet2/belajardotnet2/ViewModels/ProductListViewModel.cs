﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class ProductListViewModel
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string CompanyName { get; set; }

        public decimal UnitPrice { get; set; }
        public short Quantity { get; set; }
        public decimal? total { get; set; }
        public float Discount { get; set; }


        public ProductListViewModel(Order_Detail item) //Order_Detail dari entity
        {
            ProductID = item.ProductID;
            ProductName = item.Product.ProductName;
            CategoryName = item.Product.Category.CategoryName;
            CompanyName = item.Product.Supplier.CompanyName;
            UnitPrice = item.UnitPrice;
            Quantity = item.Quantity;
            total = (Convert.ToDecimal(item.UnitPrice) * Convert.ToDecimal(item.Quantity))-(Convert.ToDecimal(item.UnitPrice) * Convert.ToDecimal(item.Quantity) * Convert.ToDecimal(item.Discount));

        }
    }
}