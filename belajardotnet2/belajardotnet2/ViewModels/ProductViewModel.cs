﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class ProductViewModel
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int? SupplierID { get; set; }
        public int? CategoryID { get; set; }

        public string QuantityPerUnit { get; set;}

        public decimal? UnitPrice { get; set; }

        public short? UnitsInStock { get; set; }

        public short? UnitsOnOrder { get; set; }

        public short? ReorderLevel { get; set; }

        public bool Discontinued { get; set; }

        public ProductViewModel (Product item)
        {
            ProductID = item.ProductID;
            ProductName = item.ProductName;
            QuantityPerUnit = item.QuantityPerUnit;
            UnitPrice = item.UnitPrice;
            UnitsInStock = item.UnitsInStock;
            UnitsOnOrder = item.UnitsOnOrder;
            ReorderLevel = item.ReorderLevel;
            Discontinued = item.Discontinued;
            SupplierID = item.SupplierID;
            CategoryID = item.CategoryID;
        }

        public ProductViewModel()
        {

        }
    }
}