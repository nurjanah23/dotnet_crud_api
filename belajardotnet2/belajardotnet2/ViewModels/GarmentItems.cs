﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class GarmentItems 
    {
        int ProductID { get; set; }
        string ProductDescription { get; set; }
        string ProductionCode { get; set; }
        string ProductionDate { get; set; }
        string GarmentsType { get; set; }
        string Fabrics { get; set; }
        string GenderRelated { get; set; }
        string IsWaterProof { get; set; }
        string Color { get; set; }
        string Size { get; set; }
        string AgeGroup { get; set; }

        public GarmentItems()
        {

        }

        public Dictionary<string, object> garment()
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("ProductID", this.ProductID);
            data.Add("ProductDescription", this.ProductDescription);
            data.Add("ProductionCode", this.ProductionCode);
            data.Add("ProductionDate", this.ProductionDate);
            data.Add("GarmentsType", this.GarmentsType);
            data.Add("Fabrics", this.Fabrics);
            data.Add("GenderRelated", this.GenderRelated);
            data.Add("IsWaterProof", this.IsWaterProof);
            data.Add("Color", this.Color);
            data.Add("Size", this.Size);
            data.Add("AgeGroup", this.AgeGroup);
            return data;
        }

        public GarmentItems(product product)
        {
            char[] delimiter = { '|' };
            this.ProductID = product.ProductID;

            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] dataProduct = product.ProductDetail.Split(delimiter);

                this.ProductDescription = dataProduct[0];
                this.ProductionCode = dataProduct[1];
                this.ProductionDate = dataProduct[2];
                this.GarmentsType = dataProduct[3];
                this.Fabrics = dataProduct[4];
                this.GenderRelated = dataProduct[5];
                this.IsWaterProof = dataProduct[6];
                this.Color = dataProduct[7];
                this.Size = dataProduct[8];
                this.AgeGroup = dataProduct[9];
            }
        }
        public string detailData()
        {
            return
                this.ProductID + "|" +
                this.ProductDescription + "|" +
                this.ProductionCode + "|" +
                this.ProductionDate + "|" +
                this.GarmentsType + "|" +
                this.Fabrics + "|" +
                this.GenderRelated + "|" +
                this.IsWaterProof + "|" +
                this.Color + "|" +
                this.Size + "|" +
                this.AgeGroup;
        }

    }
}