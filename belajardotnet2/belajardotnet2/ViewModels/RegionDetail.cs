﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class RegionDetail
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public string RegionLangitude { get; set; }
        public string RegionLatitude { get; set; }
        public string Country { get; set; }



        public RegionDetail()
        {

        }

        public RegionDetail(Region region)
        {
            char[] delimiter = { '|' };
            this.RegionID = region.RegionID;

            if (!string.IsNullOrEmpty(region.RegionDescription))
            {
                String[] regionDetailData = region.RegionDescription.Split(delimiter);

                if (regionDetailData.Length == 4)
                {
                    this.RegionName = regionDetailData[0];
                    this.RegionLangitude = regionDetailData[1];
                    this.RegionLatitude = regionDetailData[2];
                    this.Country = regionDetailData[3];
                }
            }
        }
        public Region convertToRegion()
        {
            char[] delimiter = { '|' };
            return new Region()
            {
                RegionID = this.RegionID,
                RegionDescription =
                    this.RegionName + delimiter[0] +
                    this.RegionLangitude + delimiter[0] +
                    this.RegionLatitude + delimiter[0] +
                    this.Country,
            };
        }

        public Territory tambahTerritory(DB_Context db)
        {
            if (this.Country.Contains("INA"))
            {
                Territory xx = new Territory()
                {
                    TerritoryID = "INA-01",
                    TerritoryDescription = "Bandung wilayah yang berada di Indonesia",
                    RegionID = this.RegionID,
                };
                db.Territories.Add(xx);
                db.SaveChanges();
            }
            return new Territory();
        }

        public Region dataRegion(DB_Context x)
        {
            
            var listRegion = x.Regions.Where(data => data.RegionID == this.RegionID).AsEnumerable().ToList();
            List<RegionDetail> dataRegions = new List<RegionDetail>();
            foreach (var item in listRegion)
            {
                RegionDetail ListDataRegion = new RegionDetail(item);
                dataRegions.Add(ListDataRegion);
            }
            return new Region();
            //result.Add("Data", dataRegion);
        }
    }
}