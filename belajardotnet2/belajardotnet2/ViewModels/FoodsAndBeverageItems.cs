﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class FoodsAndBeverageItems 
    {
        int ProductID { get; set; }
        string ProductDescription { get; set; }
        string ProductionCode { get; set; }
        string ProductionDate { get; set; }
        string ExpiredDate { get; set; }
        string NetWeight { get; set; }
        string Ingredients { get; set; }
        string DailyValue { get; set; }
        string Certification { get; set; }

        public FoodsAndBeverageItems()
        {

        }

        public Dictionary<string, object> foods()
        {
            Dictionary<string, object> dataFoods = new Dictionary<string, object>();
            dataFoods.Add("ProductID", this.ProductID);
            dataFoods.Add("ProductDescription", this.ProductDescription);
            dataFoods.Add("ProductionCode", this.ProductionCode);
            dataFoods.Add("ProductionDate", this.ProductionDate);
            dataFoods.Add("ExpiredDate", this.ExpiredDate);
            dataFoods.Add("NetWeight", this.NetWeight);
            dataFoods.Add("Ingredients", this.Ingredients);
            dataFoods.Add("DailyValue", this.DailyValue);
            dataFoods.Add("Certification", this.Certification);
            return dataFoods;
        }

        public FoodsAndBeverageItems(product product)
        {
            char[] delimiter = { '|' };
            this.ProductID = product.ProductID;

            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] dataProduct = product.ProductDetail.Split(delimiter);

                    this.ProductDescription = dataProduct[0];
                    this.ProductionCode = dataProduct[1];
                    this.ProductionDate = dataProduct[2];
                    this.ExpiredDate = dataProduct[3];
                    this.NetWeight = dataProduct[4];
                    this.Ingredients = dataProduct[5];
                    this.DailyValue = dataProduct[6];
                    this.Certification = dataProduct[7];
            }
        }
        public string detailData()
        {
                return 
                    this.ProductDescription + "|" +
                    this.ProductionCode + "|" +
                    this.ProductionDate + "|" +
                    this.ExpiredDate + "|" +
                    this.NetWeight + "|" +
                    this.Ingredients + "|" +
                    this.DailyValue + "|" +
                    this.Certification ;
        }


    }
    
}