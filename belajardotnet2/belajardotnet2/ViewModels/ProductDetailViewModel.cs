﻿using AutoMapper;
using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class ProductDetailViewModel : ProductViewModel
    {
  
        public ProductDetailViewModel()
        {

        }

        public ProductDetailViewModel(product product)
        {
            ProductID = product.ProductID;
            ProductName = product.ProductName;
            SupplierID = product.SupplierID;
            CategoryID = product.CategoryID;
            QuantityPerUnit = product.QuantityPerUnit;
            UnitPrice = product.UnitPrice;
            UnitsInStock = product.UnitsInStock;
            UnitsOnOrder = product.UnitsOnOrder;
            ReorderLevel = product.ReorderLevel;
            Discontinued = product.Discontinued;
            ProductType = product.ProductType;
            if (ProductType != null)
            {
                switch (ProductType)
                {
                    case "FoodsAndBeverageItems":
                        FoodsAndBeverageItems dataFoods = new FoodsAndBeverageItems(product);
                        ProductDetail = dataFoods.foods();
                        break;
                    case "GarmentItems":
                        GarmentItems dataGarment = new GarmentItems(product);
                        ProductDetail = dataGarment.garment();
                        break;
                    case "MaterialItems":
                        MaterialItems dataMaterial = new MaterialItems(product);
                        ProductDetail = dataMaterial.material();
                        break;
                    case "TransportationServices":
                        TransportationServices dataTransportation = new TransportationServices(product);
                        ProductDetail = dataTransportation.transportation();
                        break;
                }
            }
            else
            {
                ProductDetail = null;
            }
            
        }

        public product convertToProduct()
        {
            var dataDetail = "";
            var con = new MapperConfiguration(kon => {});
            var map = new Mapper(con);

            switch (this.ProductType)
            {
                case "FoodsAndBeverageItems":
                    FoodsAndBeverageItems foods = map.Map<FoodsAndBeverageItems>(this.ProductDetail);
                    dataDetail = foods.detailData();
                    break;
                case "GarmentItems":
                    GarmentItems garment = map.Map<GarmentItems>(this.ProductDetail);
                    dataDetail = garment.detailData();
                    break;
                case "MaterialItems":
                    MaterialItems material = map.Map<MaterialItems>(this.ProductDetail);
                    dataDetail = material.detailData();
                    break;
                case "TransportationServices":
                    TransportationServices transportation = map.Map<TransportationServices>(this.ProductDetail);
                    dataDetail = transportation.detailData();
                    break;

            }
            
            //if (this.ProductType.Contains("FoodsAndBeverageItems"))
            //{
            //    FoodsAndBeverageItems foods = map.Map<FoodsAndBeverageItems>(this.ProductDetail);
            //    dataDetail = foods.detailData();
            //}
            //else if (this.ProductType.Contains("GarmentItems"))
            //{
            //    GarmentItems garment = map.Map<GarmentItems>(this.ProductDetail);
            //    dataDetail = garment.detailData();
            //}
            //else if (this.ProductType.Contains("MaterialItems"))
            //{
            //    MaterialItems material = map.Map<MaterialItems>(this.ProductDetail);
            //    dataDetail = material.detailData();
            //}
            //else if (this.ProductType.Contains("TransportationServices"))
            //{
            //    TransportationServices transportation = map.Map<TransportationServices>(this.ProductDetail);
            //    dataDetail = transportation.detailData();
            //}

            return new product()
            {
                ProductID = this.ProductID,
                ProductName = this.ProductName,
                SupplierID = this.SupplierID,
                CategoryID = this.CategoryID,
                QuantityPerUnit = this.QuantityPerUnit,
                UnitPrice = this.UnitPrice,
                UnitsInStock = this.UnitsInStock,
                UnitsOnOrder = this.UnitsOnOrder,
                ReorderLevel = this.ReorderLevel,
                Discontinued = this.Discontinued,
                ProductType = this.ProductType,
                ProductDetail = dataDetail,
            };
        }

    } 
       
    
}