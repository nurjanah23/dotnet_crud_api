﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class MaterialItems 
    {
       
        int ProductID { get; set; }
        string ProductDescription { get; set; }
        string ProductionCode { get; set; }
        string ProductionDate { get; set; }
        string ExpiredDate { get; set; }
        string MaterialsType { get; set; }
        string UnitOfMeasurement { get; set; }
        string IsConsumable { get; set; }

        public MaterialItems()
        {

        }

        public Dictionary<string, object> material()
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("ProductID", this.ProductID);
            data.Add("ProductDescription", this.ProductDescription);
            data.Add("ProductionCode", this.ProductionCode);
            data.Add("ProductionDate", this.ProductionDate);
            data.Add("ExpiredDate", this.ExpiredDate);
            data.Add("MaterialsType", this.MaterialsType);
            data.Add("UnitOfMeasurement", this.UnitOfMeasurement);
            data.Add("IsConsumable", this.IsConsumable);
            return data;
        }

        public MaterialItems(product product)
        {
            char[] delimiter = { '|' };
            this.ProductID = product.ProductID;

            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                String[] dataProduct = product.ProductDetail.Split(delimiter);

                this.ProductDescription = dataProduct[0];
                this.ProductionCode = dataProduct[1];
                this.ProductionDate = dataProduct[2];
                this.ExpiredDate = dataProduct[3];
                this.MaterialsType = dataProduct[4];
                this.UnitOfMeasurement = dataProduct[5];
                this.IsConsumable = dataProduct[6];
            }
        }

        public string detailData()
        {
            return
                this.ProductDescription + "|" +
                this.ProductionCode + "|" +
                this.ProductionDate + "|" +
                this.ExpiredDate + "|" +
                this.MaterialsType + "|" +
                this.UnitOfMeasurement + "|" +
                this.IsConsumable;

        }
    }
}