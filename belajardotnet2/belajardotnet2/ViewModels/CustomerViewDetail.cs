﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class CustomerViewDetail
    {
        public string CustomerID { get; set; }
        
        public List<OrderDetailViewModel> orderDetail  { get; set; }
        public decimal? totalOrder { get; set; }

        public decimal? jumlahOrder { get; set; }


        public CustomerViewDetail( Customer x)
        {
            CustomerID = x.CustomerID;
            orderDetail = x.Orders.Select(data => new OrderDetailViewModel(data)).ToList();
            totalOrder = x.Orders.Select(data => new OrderDetailViewModel(data)).ToList().Sum(data => data.grandTotal);
            jumlahOrder = x.Orders.Select(data => new OrderDetailViewModel(data)).ToList().Count();

        }

    }
}