﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class RegionViewModel
    {
        public int RegionID { get; set; }
       
        public string RegionName { get; set; }

        public string RegionLangitude { get; set; }

        public string RegionLatitude { get; set; }

        public string Country { get; set; }

        public RegionViewModel()
        {

        }

        public RegionViewModel(Region item)
        {
            RegionID = item.RegionID;
            string[] pemisah = item.RegionDescription.Split('|');

            RegionName = pemisah[0];
            RegionLangitude = pemisah[1];
            RegionLatitude = pemisah[2];
            Country = pemisah[3];
        }

    }
}