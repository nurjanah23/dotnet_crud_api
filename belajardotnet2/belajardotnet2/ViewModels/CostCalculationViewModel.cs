﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class CostCalculationViewModel
    {
        public int productID { get; set; }
        public string productName { get; set; }
        public string costCalculationMethod { get; set; }
        public int costCalculation { get; set; }
        private int costRate { get; set; }
        private int routeMilleage { get; set; }


        public CostCalculationViewModel(product product, string condition, int userDemand)
        {
            char[] delimiter = { '|' };
            this.productID = product.ProductID;
            if (!string.IsNullOrEmpty(product.ProductDetail))
            {
                string[] dataProduct = product.ProductDetail.Split(delimiter);

                this.productName = product.ProductName;
                this.routeMilleage = Int32.Parse(dataProduct[3]);
                this.costCalculationMethod = dataProduct[4];
                this.costRate = Int32.Parse(dataProduct[5]);
                this.costCalculation = calculation(condition, userDemand);

            }
        }

        public int calculation(string condition, int userDemand)
        {
            var hasil = 0;
            if (costCalculationMethod.Contains("FixPerRoute"))
            {
                hasil = 1 * costRate;
            }
            if (costCalculationMethod.Contains("PerMiles"))
            {
                hasil = routeMilleage * (costRate / 2);
            }
            if (costCalculationMethod.Contains("PerMilesWithCondition"))
            {
                var nilai = 0;
                if (condition == "GoodWeather")
                {
                    nilai = 5;
                }
                else if (condition == "BadWeather")
                {
                    nilai = 15;
                }
                hasil = (routeMilleage * costRate / 2) * (((nilai + (userDemand / 50)) + 95 / 100));
            }
            return hasil;
        }
    }
}