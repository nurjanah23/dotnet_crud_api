﻿using belajardotnet2.EntityFramworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace belajardotnet2.ViewModels
{
    public class OrderDetailViewModel
    {
        public int? OrderID { get; set; }

        public string ContactName { get; set; }

        public decimal? grandTotal { get; set; }
        
        
        public List<ProductListViewModel> listProduct { get; set; }

        public OrderDetailViewModel(Order item) /// Order adalah entity
        {
            OrderID = item.OrderID;
            ContactName = item.Customer.ContactName;
            listProduct = item.Order_Details.ToList().Select(data => new ProductListViewModel(data)).ToList();
            grandTotal = item.Order_Details.ToList().Select(data => new ProductListViewModel(data)).ToList().Sum(data => data.total);
        }



    }
}